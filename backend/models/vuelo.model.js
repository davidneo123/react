const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const vueloSchema = new Schema({
    vuelo: { type: String, required: true },
    ciudadInicio: { type: String, required: true },
    ciudadDestino: { type: String, required: true },
    duracion: { type: Number, required: true },
    fecha: {
        type: Date
    },
    /* updated_at: {
        type: Date,
        default: Date.now
    }, */
}, {
    timestamps: true,
});

const Vuelo = mongoose.model('Vuelo', vueloSchema);

module.exports = Vuelo;