const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reservaSchema = new Schema({
    cedula: { type: String, required: true },
    nombre: { type: String, required: true },
    vuelo: { type: mongoose.Schema.Types.ObjectId },
}, {
    timestamps: true,
});

const Reserva = mongoose.model('Reserva', reservaSchema);

module.exports = Reserva;