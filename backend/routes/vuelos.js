const router = require('express').Router();
let Vuelo = require('../models/vuelo.model');
var mongoose = require('mongoose');

router.route('/').get((req, res) => {
    Vuelo.find()
        .then(vuelos => res.json(vuelos).status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});


router.route('/:id').get((req, res) => {
    // let id = mongoose.Types.ObjectId(req.body.id)
    var id = req.params.id;
    Vuelo.findById(id, function(err, resource) {
        if (err) {
            return res.send(err).status(501);
        } else {
            res.json(resource).status(201);
        }
    })
})

router.route('/add').post((req, res) => {
    console.log('salvando')
    const vuelo = req.body.vuelo;
    const ciudadInicio = req.body.ciudadInicio;
    const ciudadDestino = req.body.ciudadDestino;
    const fecha = req.body.fecha;
    const duracion = req.body.duracion;

    const newVuelo = new Vuelo({
        vuelo,
        ciudadInicio,
        ciudadDestino,
        fecha,
        duracion
    });
    console.log('salvando: ' + newVuelo)
    newVuelo.save()
        .then(() => res.json('Vuelo added!').status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;