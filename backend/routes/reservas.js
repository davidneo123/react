const router = require('express').Router();
let Reserva = require('../models/reserva.model');
var mongoose = require('mongoose');

router.route('/').get((req, res) => {
    Reserva.find()
        .then(Reservas => res.json(Reservas).status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    const cedula = req.body.cedula;
    const nombre = req.body.nombre;
    const vuelo = mongoose.Types.ObjectId(req.body.vuelo);


    const newReserva = new Reserva({
        cedula,
        nombre,
        vuelo
    });

    newReserva.save()
        .then(() => res.json('Reserva added!').status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
    Reserva.findById(req.params.id)
        .then(Reserva => res.json(Reserva).status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
    Reserva.findByIdAndDelete(req.params.id)
        .then(() => res.json('Reserva deleted.').status(200))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
    Reserva.findById(req.params.id)
        .then(Reserva => {
            Reserva.cedula = req.body.cedula;
            Reserva.nombre = req.body.nombre;
            Reserva.vuelo = mongoose.Types.ObjectId(req.body.vuelo);
            Reserva.save()
                .then(() => res.json('Reserva updated!').status(200))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;