import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Navbar from "./components/navbar.component"
import ListarReservas from "./components/listar-reservas.component";
import EditarReserva from "./components/editar-reserva.component";
import CrearReserva from "./components/crear-reserva.component";
import CrearVuelo from "./components/crear-vuelo.component";

function App() {
    return ( <Router>
        <div className = "container" >
        <Navbar/>
        <br/>
        <Route path = "/" exact component = { ListarReservas }/> 
        <Route path = "/edit/:id" component = { EditarReserva }/>
        <Route path = "/reserva" component = { CrearReserva }/> 
        <Route path = "/vuelo" component = { CrearVuelo }/> 
        </div> 
        </Router>
    );
}

export default App;