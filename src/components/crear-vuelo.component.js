import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class CreateVuelo extends Component {
  constructor(props) {
    super(props);

    this.onChangeVuelo = this.onChangeVuelo.bind(this);
    this.onChangeCiudadInicio = this.onChangeCiudadInicio.bind(this);
    this.onChangeCiudadDestino = this.onChangeCiudadDestino.bind(this);
    this.onChangeDuracion = this.onChangeDuracion.bind(this);
    this.onChangeFecha = this.onChangeFecha.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      vuelo: '',
      duracion: 0,
      fecha: new Date(),
      ciudadInicio:'',
      ciudadDestino:''
    }
  }

  onChangeVuelo(e) {
    this.setState({
      vuelo: e.target.value
    })
  }

   onChangeCiudadInicio(e) {
    this.setState({
      ciudadInicio: e.target.value
    })
  }
   
   onChangeCiudadDestino(e) {
    this.setState({
      ciudadDestino: e.target.value
    })
  }

  onChangeDuracion(e) {
    this.setState({
      duracion: e.target.value
    })
  }

  onChangeFecha(fecha) {
    this.setState({
      fecha: fecha
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const vuelo = {
      vuelo: this.state.vuelo,
      ciudadInicio: this.state.ciudadInicio,
      ciudadDestino:this.state.ciudadDestino,
      fecha:this.state.fecha,
      duracion:this.state.duracion
    }
  console.log(vuelo)
    axios.post('http://localhost:5000/vuelos/add', vuelo)
      .then(res => console.log(res.data));

    this.setState({
      vuelo: '',
      duracion:0,
      fecha:new Date(),
      ciudadInicio:'',
      ciudadDestino:''
    })
  }

  render() {
    return (
      <div>
        <h3>Nuevo Vuelo</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group"> 
            <label>Número de vuelo: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.vuelo}
                onChange={this.onChangeVuelo}
                />
          </div>
          <div className="form-group"> 
            <label>Ciudad Origen: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.ciudadInicio}
                onChange={this.onChangeCiudadInicio}
                />
          </div>
          <div className="form-group"> 
            <label>Ciudad Destino: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.ciudadDestino}
                onChange={this.onChangeCiudadDestino}
                />
          </div>
          <div className="form-group">
            <label>Fecha: </label>
            <div>
              <DatePicker
                selected={this.state.fecha}
                onChange={this.onChangeFecha}
              />
            </div>
          </div>
          <div className="form-group"> 
            <label>Duración del vuelo: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.duracion}
                onChange={this.onChangeDuracion}
                />
          </div>
          <div className="form-group">
            <input type="submit" value="Crear Vuelo" className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}