import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class EditarReserva extends Component {
  constructor(props) {
    super(props);

    this.onChangeVuelo = this.onChangeVuelo.bind(this);
    this.onChangeCedula = this.onChangeCedula.bind(this);
    this.onChangeNombre = this.onChangeNombre.bind(this);
    this.onChangeFecha = this.onChangeFecha.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      vuelo: '',
      cedula: '',
      nombre: '',
      fecha: new Date(),
      vuelos: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/reservas/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          vuelo: response.data.vuelo,
          cedula: response.data.cedula,
          nombre: response.data.nombre,
          fecha: new Date(response.data.fecha)
        })   
      })
      .catch(function (error) {
        console.log(error);
      })

    axios.get('http://localhost:5000/vuelos/')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
            users: response.data.map(vuelo => vuelo.vuelo),
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })

  }

  onChangeVuelo(e) {
    this.setState({
      vuelo: e.target.value
    })
  }

  onChangeCedula(e) {
    this.setState({
      cedula: e.target.value
    })
  }

  onChangeNombre(e) {
    this.setState({
      nombre: e.target.value
    })
  }

  onChangeFecha(fecha) {
    this.setState({
      fecha: fecha
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const reserva = {
      vuelo: this.state.vuelo,
      cedula: this.state.cedula,
      nombre: this.state.nombre,
      fecha: this.state.fecha
    }

    console.log(reserva);

    axios.post('http://localhost:5000/reserva/update/' + this.props.match.params.id, reserva)
      .then(res => console.log(res.data));

    window.location = '/';
  }

  render() {
    return (
    <div>
      <h3>Editar Reserva</h3>
      <form onSubmit={this.onSubmit}>
        <div className="form-group"> 
          <label>Vuelo: </label>
          <select ref="vueloInput"
              required
              className="form-control"
              value={this.state.vuelo}
              onChange={this.onChangeVuelo}>
              {
                this.state.vuelos.map(function(vuelo) {
                  return <option 
                    key={vuelo}
                    value={vuelo}>{vuelo}
                    </option>;
                })
              }
          </select>
        </div>
        <div className="form-group"> 
          <label>Cedula: </label>
          <input  type="text"
              required
              className="form-control"
              value={this.state.cedula}
              onChange={this.onChangeCedula}
              />
        </div>
        <div className="form-group">
          <label>Nombre Completo: </label>
          <input 
              type="text" 
              className="form-control"
              value={this.state.nombre}
              onChange={this.onChangeNombre}
              />
        </div>
        <div className="form-group">
          <label>Fecha: </label>
          <div>
            <DatePicker
              selected={this.state.fecha}
              onChange={this.onChangeFecha}
            />
          </div>
        </div>

        <div className="form-group">
          <input type="submit" value="Editar Reserva" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}