import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Tech and Fly</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
          <Link to="/" className="nav-link">Reservas</Link>
          </li>
          <li className="navbar-item">
          <Link to="/reserva" className="nav-link">Realizar reservación </Link>
          </li>
          <li className="navbar-item">
          <Link to="/vuelo" className="nav-link">Crear vuelo</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}