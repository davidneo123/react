import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Reserva = props => (
  <tr>
    <td>{props.reserva.cedula}</td>
    <td>{props.reserva.nombre}</td>
    <td>{props.reserva.vuelo}</td>
    <td>{props.reserva.createdAt.substring(0,10)}</td>
    <td>
      <Link to={"/edit/"+props.reserva._id}>edit</Link> | <a href="#" onClick={() => { props.deleteReserva(props.reserva._id) }}>delete</a>
    </td>
  </tr>
)

export default class ReservasList extends Component {
  constructor(props) {
    super(props);

    this.deleteReserva = this.deleteReserva.bind(this)

    this.state = {reservas: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/reservas/')
      .then(response => {
        this.setState({ reservas: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  getVuelo(id){
     axios.get('http://localhost:5000/vuelos/'+id)
      .then(response => {
              
              return response.data.vuelo
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteReserva(id) {
    axios.delete('http://localhost:5000/reservas/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      reservas: this.state.reservas.filter(el => el._id !== id)
    })
  }

 reservaList() {
    return this.state.reservas.map(currentreserva => {
      return <Reserva reserva={currentreserva} deleteReserva={this.deleteReserva} key={currentreserva._id}/>;
    })
  }

  render() {
    return (
      <div>
        <h3>Reservas</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>cedula</th>
              <th>nombre</th>
              <th>vuelo</th>
              <th>Fecha</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            { this.reservaList() }
          </tbody>
        </table>
      </div>
    )
  }
}